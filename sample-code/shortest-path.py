from pythonds.graphs import PriorityQueue, Graph, Vertex
def dijkstra(aGraph,start):
 pq = PriorityQueue()
 start.setDistance(0)
 pq.buildHeap([(v.getDistance(),v) for v in aGraph])
 while not pq.isEmpty():
  currentVert = pq.delMin()
  for nextVert in currentVert.getConnections():
   newDist = currentVert.getDistance() \
     + currentVert.getWeight(nextVert)
   if newDist < nextVert.getDistance():
    nextVert.setDistance( newDist )
    nextVert.setPred(currentVert)
    pq.decreaseKey(nextVert,newDist)

## BELLMAN FORD
# Step 1: For each node prepare the destination and predecessor
def bel_initialize(graph, source):
  d = {} # Stands for destination
  p = {} # Stands for predecessor
  for node in graph:
    d[node] = float('Inf') # We start admiting that the rest 
                           # of nodes are very very far
    p[node] = None
  d[source] = 0 # For the source we know how to reach
  return d, p
def bel_relax(node, neighbour, graph, d, p):
  if d[neighbour] > d[node] + graph[node][neighbour]:
    # Record this lower distance
    d[neighbour]  = d[node] + graph[node][neighbour]
    p[neighbour] = node
def bellman_ford(graph, source):
  d, p = bel_initialize(graph, source)
  for i in range(len(graph)-1): #Run this until is converges
    for u in graph:
      for v in graph[u]: #For each neighbour of u
        bel_relax(u, v, graph, d, p) #Lets bel_relax it
  # Step 3: check for negative-weight cycles
  for u in graph:
    for v in graph[u]:
      assert d[v] <= d[u] + graph[u][v]
  return d, p