def multiply(x, y):
  if x.bit_length() <= _CUTOFF or y.bit_length() <= _CUTOFF:
    return x * y
  else:
    n = max(x.bit_length(), y.bit_length())
    half = (n + 32) // 64 * 32
    mask = (1 << half) - 1
    xlow = x & mask
    ylow = y & mask
    xhigh = x >> half
    yhigh = y >> half
    
    a = multiply(xhigh, yhigh)
    b = multiply(xlow + xhigh, ylow + yhigh)
    c = multiply(xlow, ylow)
    d = b - a - c
    return (((a << half) + d) << half) + c


def prim(G,start):
 pq = PriorityQueue()
 for v in G:
  v.setDistance(sys.maxsize)
  v.setPred(None)
 start.setDistance(0)
 pq.buildHeap([(v.getDistance(),v) for v in G])
 while not pq.isEmpty():
  currentVert = pq.delMin()
  for nextVert in currentVert.getConnections():
    newCost = currentVert.getWeight(nextVert) \
      + currentVert.getDistance()
    if v in pq and newCost<nextVert.getDistance():
     nextVert.setPred(currentVert)
     nextVert.setDistance(newCost)
     pq.decreaseKey(nextVert,newCost)