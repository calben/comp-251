
import java.io.*;
import java.util.*;

public class FordFulkerson
{

    /**
     * Pseudocode from Wikipedia employed
     * http://en.wikipedia.org/wiki/Depth-first_search#Pseudocode An iterative
     * edition of depth first search is employed 1 procedure DFS-iterative(G,v):
     * 2 let S be a stack 3 S.push(v) 4 while S is not empty 5 v = S.pop() 6 if
     * v is not labeled as discovered: 7 label v as discovered 8 for all edges
     * from v to w in G.adjacentEdges(v) do 9 S.push(w)
     *
     *
     * @param source
     * @param destination
     * @param graph
     * @return
     */
    public static ArrayList<Integer> pathDFS(Integer source, Integer destination, WGraph graph)
    {
        Stack<Integer> stack = new Stack<Integer>();
        Stack<Integer> tmp = new Stack<Integer>();
        tmp.add(source);
        stack.add(source);
        while (tmp.size() > 0)
        {
            Integer current_node_value = tmp.remove(0);
            if (current_node_value == destination)
            {
                break;
            }
            for (int i = graph.listOfEdgesSorted().size() - 1; i >= 0; i--)
            {
                Edge e = graph.getEdges().get(i);
                if (current_node_value == e.nodes[0])
                {
                    if (!stack.contains(e.nodes[1]) && e.weight != 0)
                    {
                        tmp.add(e.nodes[1]);
                        stack.add(e.nodes[1]);
                        break;
                    }
                }
            }
        }
        return new ArrayList(stack);
    }

    /**
     * BASED ON PYTHON PSEUDOCODE
     *
     *
     * def find_path(self, source, sink, path): if source == sink: return path
     * for edge in self.get_edges(source): residual = edge.capacity -
     * self.flow[edge] if residual > 0 and edge not in path: result =
     * self.find_path( edge.sink, sink, path + [edge]) if result != None: return
     * result
     *
     * def max_flow(self, source, sink): path = self.find_path(source, sink, [])
     * while path != None: residuals = [edge.capacity - self.flow[edge] for edge
     * in path] flow = min(residuals) for edge in path: self.flow[edge] += flow
     * self.flow[edge.redge] -= flow path = self.find_path(source, sink, [])
     * return sum(self.flow[edge] for edge in self.get_edges(source))
     *
     * @param source
     * @param destination
     * @param graph
     * @param filePath
     */
    public static void fordfulkerson(Integer source, Integer destination, WGraph graph, String filePath)
    {
        String answer = "";
        String mcgill_id = "260467886";
        int max_flow = 0;

        // STEP 1: GET AUGMENTING PATH
        WGraph residual = new WGraph(graph);

        // STEP 2: SET ALL WEIGHTS TO 0
        for (Edge e : graph.getEdges())
        {
            e.weight = 0;
        }

        ArrayList<Integer> paths_by_depth = pathDFS(source, destination, residual);

        while (!(paths_by_depth.contains(residual.getSource()) && paths_by_depth.size() == 1))
        {
            if (paths_by_depth.contains(residual.getDestination()))
            {
                int minimum = residual.getEdge(paths_by_depth.get(0), paths_by_depth.get(1)).weight;
                for (int i = 0; i < paths_by_depth.size() - 1; i++)
                {
                    if (residual.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight < minimum && residual.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight != 0)
                    {
                        minimum = residual.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight;
                    }
                }
                max_flow += minimum;
                for (int i = 0; i < paths_by_depth.size() - 1; i++)
                {
                    graph.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight += minimum;
                    residual.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight -= minimum;
                }
            }

            if (!paths_by_depth.contains(residual.getDestination()) && !(paths_by_depth.contains(residual.getSource()) && paths_by_depth.size() == 1))
            {
                for (int i = 0; i < paths_by_depth.size() - 1; i++)
                {
                    residual.getEdge(paths_by_depth.get(i), paths_by_depth.get(i + 1)).weight = 0;
                }
            }
            paths_by_depth = pathDFS(source, destination, residual);
        }

        answer += max_flow + "\n" + graph.toString();
        writeAnswer(filePath + mcgill_id + ".txt", answer);
        System.out.println(answer);
    }

    public static void writeAnswer(String path, String line)
    {
        BufferedReader br = null;
        File file = new File(path);
        // if file doesnt exists, then create it

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line + "\n");
            bw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            } catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args)
    {
        String file = args[0];
        File f = new File(file);
        WGraph g = new WGraph(file);
        fordfulkerson(g.getSource(), g.getDestination(), g, f.getAbsolutePath().replace(".txt", ""));
    }
}
