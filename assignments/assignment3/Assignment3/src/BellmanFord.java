
import java.util.*;

public class BellmanFord
{

    private int[] distances = null;
    private int[] predecessors = null;
    private int source;

    /**
     * HEAVILY ADVISED BY function BellmanFord(list vertices, list edges, vertex
     * source) ::distance[],predecessor[]
     *
     * // This implementation takes in a graph, represented as // lists of
     * vertices and edges, and fills two arrays // (distance and predecessor)
     * with shortest-path // (less cost/distance/metric) information
     *
     * // Step 1: initialize graph for each vertex v in vertices: if v is
     * source then distance[v] := 0 else distance[v] := inf predecessor[v] :=
     * null
     *
     * // Step 2: relax edges repeatedly for i from 1 to size(vertices)-1: for
     * each edge (u, v) with weight w in edges: if distance[u] + w <
     * distance[v]: distance[v] := distance[u] + w predecessor[v] := u
     *
     * // Step 3: check for negative-weight cycles for each edge (u, v) with
     * weight w in edges: if distance[u] + w < distance[v]: error "Graph
     * contains a negative-weight cycle" return distance[], predecessor[] @param
     * g @param source
     */
    BellmanFord(WGraph g, int source) throws Exception
    {
        this.distances = new int[g.getNbNodes()];
        this.predecessors = new int[g.getNbNodes()];
        this.source = source;
        ArrayList<Edge> edges = new ArrayList<>();

        // STEP 1 FROM ABOVE
        for (int i = 0; i < g.getNbNodes(); i++)
        {
            if (i == source)
            {
                this.distances[i] = 0;
            } else
            {
                this.distances[i] = Integer.MAX_VALUE;
            }
            this.predecessors[i] = -1;
        }

        // STEP 2 FROM ABOVE
        for (int i = 0; i < g.getNbNodes(); i++)
        {
            for (Edge e : g.getEdges())
            {
                if (e.nodes[0] == i)
                {
                    if (this.distances[i] + e.weight < this.distances[e.nodes[1]])
                    {
                        this.distances[e.nodes[1]] = e.weight + this.distances[i];
                        this.predecessors[e.nodes[1]] = i;
                    }
                }
            }
        }

        // STEP 3 FROM ABOVE
        for (Edge e : g.getEdges())
        {
            // checking for invalid result
            if (this.distances[e.nodes[0]] + e.weight < this.distances[e.nodes[1]])
            {
                throw new Exception();
            }
        }

    }

    /**
     * @param destination
     * @return
     */
    public int[] shortestPath(int destination) throws Exception
    {
        int tmp = destination;
        ArrayList<Integer> path_tmp = new ArrayList<>();
        path_tmp.add(destination);
        while (tmp != source)
        {
            if (this.predecessors[tmp] == -1 || this.distances[tmp] == Integer.MAX_VALUE)
            {
                throw new Exception();
            } else
            {
                path_tmp.add(this.predecessors[tmp]);
                tmp = this.predecessors[tmp];
            }
        }
        int[] path = new int[path_tmp.size()];
        for (int i = 0; i < path_tmp.size(); i++)
        {
            path[i] = path_tmp.get(path_tmp.size() - 1 - i);
        }
        return path;
    }

    public void printPath(int destination)
    {
        /*Print the path in the format s->n1->n2->destination
         *if the path exists, else catch the Error and
         *prints it
         */
        try
        {
            int[] path = this.shortestPath(destination);
            for (int i = 0; i < path.length; i++)
            {
                int next = path[i];
                if (next == destination)
                {
                    System.out.println(destination);
                } else
                {
                    System.out.print(next + "-->");
                }
            }
        } catch (Exception e)
        {
            System.out.println(e);
        }
    }

    public static void main(String[] args)
    {
        String file = args[0];
        WGraph g = new WGraph(file);
        try
        {
            BellmanFord bf = new BellmanFord(g, g.getSource());
            bf.printPath(g.getDestination());
        } catch (Exception e)
        {
            System.out.println(e);
        }

    }
}
