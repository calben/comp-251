
import java.io.*;
import java.util.*;

public class FordFulkersonAlt
{

    public static ArrayList<Integer> pathDFS(Integer source, Integer destination, WGraph graph)
    {

        ArrayList<Integer> Stack = new ArrayList<Integer>();

        ArrayList<Integer> Q = new ArrayList<Integer>(); //keep track the nodes

        Q.add(source);
        Stack.add(source);

		//System.out.println(graph.listOfEdgesSorted().get(graph.listOfEdgesSorted().size()-1).weight);
        while (Q.size() > 0)
        {
			//System.out.println("heyyyy");

            Integer v = Q.remove(0);
            if (v == destination)
            {
                break;
            }
            /*
             //break immediately once the weight of the edges from the source == 0
             if (graph.getEdges().get(0).weight == 0 || graph.getEdges().get(1).weight == 0 ) {
             break;
             }
             */
            Integer u;
            //add the closest neighbor to Queue and to the stack 
            for (int i = graph.listOfEdgesSorted().size() - 1; i >= 0; i--)
            {

                //check the closest neighbor of the vertex by the edge
                if (v == graph.getEdges().get(i).nodes[0])
                {
                    u = graph.getEdges().get(i).nodes[1];

					//if the stack doesnt contain the vertex, then add it to the stack
                    //add only the closest node to the stack
                    //if (!Q.contains(u)) {
                    //Q.add(u);
                    //}
                    if (!Stack.contains(u) && graph.getEdges().get(i).weight != 0)
                    {
                        Q.add(u);
                        Stack.add(u);
						//System.out.println("The node is: " + u);

                        break;

                    }

                }

            }
        }

		//for (int j = 0; j < Stack.size(); j++) {
        //System.out.println(Stack.get(j));
        //}
		//for (int j = 0; j < Q.size(); j++) {
        //System.out.println(Q.get(j));
        //}
		//System.out.println("End of  DFS ---------");
        return Stack;
    }

    public static boolean isAugmentingPath(ArrayList<Integer> Stack, Integer source, WGraph graph)
    {
        boolean isAugmentingPath = true;

        //return false if the stack contains only 1 node, which is the source
        if (Stack.contains(graph.getSource()) && Stack.size() == 1)
        {
            isAugmentingPath = false;
        }

        return isAugmentingPath;

    }

    public static void fordfulkerson(Integer source, Integer destination, WGraph graph, String filePath)
    {
        String answer = "";
        String myMcGillID = "260531701"; //Please initialize this variable with your McGill ID
        int maxFlow = 0;

        WGraph residual = new WGraph(graph); //get the augmenting path of the graph
        //WGraph capacity = new WGraph(graph); //get the capacity of the edges 

        //change all the weight of the original graph to 0;
        for (int i = 0; i < graph.getEdges().size(); i++)
        {
            graph.getEdges().get(i).weight = 0;
        }

        ArrayList<Integer> path = pathDFS(source, destination, residual);
        boolean b = isAugmentingPath(path, residual.getSource(), residual);

        while (b == true)
        {

		//case 1
            //if the graph has the destination, then its a complete augmenting path
            if (path.contains(residual.getDestination()))
            {
                //find the minimum weight of edges
                int minimum = residual.getEdge(path.get(0), path.get(1)).weight;
                for (int i = 0; i < path.size() - 1; i++)
                {
                    if (residual.getEdge(path.get(i), path.get(i + 1)).weight < minimum && residual.getEdge(path.get(i), path.get(i + 1)).weight != 0)
                    {
                        minimum = residual.getEdge(path.get(i), path.get(i + 1)).weight;
                    }

                }
                //update the max Flow
                maxFlow += minimum;

                //add the minimum weight in the original graph
                for (int i = 0; i < path.size() - 1; i++)
                {
                    graph.getEdge(path.get(i), path.get(i + 1)).weight += minimum;

				//System.out.println("Minimum is :" + minimum);
                }

                //subtract the minimum weight from the capacity of the residual
                for (int i = 0; i < path.size() - 1; i++)
                {
                    residual.getEdge(path.get(i), path.get(i + 1)).weight -= minimum;
                }

            }

		//case 2 (incomplete augmenting path) 
            //if the path doesnt contain the destination, but the nodes > 1 
            if (!path.contains(residual.getDestination()) && b == true)
            {

                //change all the nodes of the incomplete path to zero 
                for (int i = 0; i < path.size() - 1; i++)
                {
                    residual.getEdge(path.get(i), path.get(i + 1)).weight = 0;
                }

            }

            path = pathDFS(source, destination, residual);
            b = isAugmentingPath(path, residual.getSource(), residual);
		//System.out.println(b);

        }

        System.out.println("\n----------Residual graph --------");
        System.out.println(residual.toString());

        System.out.println("---------------------------------");
        System.out.println("Resulting Graph\n");

        answer += maxFlow + "\n" + graph.toString();
        writeAnswer(filePath + myMcGillID + ".txt", answer);
        System.out.println(answer);
    }

    public static void writeAnswer(String path, String line)
    {
        BufferedReader br = null;
        File file = new File(path);
		// if file doesnt exists, then create it

        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(line + "\n");
            bw.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            try
            {
                if (br != null)
                {
                    br.close();
                }
            } catch (IOException ex)
            {
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args)
    {
        args = new String[]
        {
            "../bf1.txt"
        };
        String file = args[0];
        File f = new File(file);
        WGraph g = new WGraph(file);
        fordfulkerson(g.getSource(), g.getDestination(), g, f.getAbsolutePath().replace(".txt", ""));
    }
}
