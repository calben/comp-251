---
title: McGill COMP 251 Assignment 02
author: Calem Bendell, 260467886
---

# Disjoint Sets

```
public int find(int i)
{
    if (par[i] == i)
    {
        return i;
    } else
    {
        par[i] = find(par[i]);
        return par[i];
    }
}

public int union(int i, int j)
{
    if (find(i) != find(j))
    {
        par[find(i)] = find(j);
    }
    return 0;
}
```


# Minimum Spanning Trees

```
public static WGraph kruskal(WGraph g)
{
    WGraph wgraph = new WGraph();
    DisjointSets set = new DisjointSets(g.getNbNodes());
    List<Edge> sortedEdges = g.listOfEdgesSorted();
    int numberOfTrees = g.getNbNodes();
    for (Edge e : sortedEdges)
    {
        if (numberOfTrees > 1 && IsSafe(set, e))
        {
            wgraph.addEdge(e);
            set.union(e.nodes[0], e.nodes[1]);
        }
    }
    return wgraph;
}

public static boolean IsSafe(DisjointSets p, Edge e)
{
    return (p.find(e.nodes[0]) != p.find(e.nodes[1]));
}
```


# Greedy Algorithms

Given points $\{x_1, x_2, \ldots, x_n\}$, give a greedy algorithm that determines the smallest set of unit length closed intervals containing all given points.

So given $\{0.5, 1.0, 1.5, 2.5, 3.2\}$, we might return 0.5 to 1.5 and 2.5 to 3.5.

The correct value can be found either by calculating the distances between all values and then calculating overlaps or by creating intervals along a sorted list of values.
We will use merge sort as our sorting algorithm.
All of heapsort, quicksort, and mergesort have worst case performance of $nlog(n)$, but merge sort is the simplest to code. 

The solution is presented in Standard ML, a miserable dialect of ML, since it would be better paractice for COMP 302 than using a better language like Lisp or Haskell.


## Demonstrative Code

```
(* val merge = fn : int list -> int list -> int list *)
fun merge xs [] = xs
  | merge [] ys = ys
  | merge (x::xs) (y::ys) = if x < y then x::(merge xs (y::ys)) else y::(merge (x::xs) ys);

(* val merge_sort = fn : int list -> int list *)
fun merge_sort [] = []
  | merge_sort [x] = [x]
  | merge_sort lst =
      let val half = length lst div 2 in merge (merge_sort (List.take (lst, half))) (merge_sort (List.drop (lst, half))) end;

(* val interval_finder = fn : real list * real * real * real * real list -> real list *)
fun interval_finder ([], prev_val, n, interval, acc) = acc
  | interval_finder (x::xs, prev_val, n, interval, acc) = 
      let
        val distance = x - prev_val
      in
        if Real.<=(n, 0.0) then interval_finder(xs, x, interval, interval, x::acc) else
          interval_finder(xs, x, (n - distance), interval, acc)
      end;

fun get_minimum_intervals (lst : real list) =
  let 
    val sorted = (merge_sort lst)
  in
    interval_finder(sorted, hd(sorted), 0.0, 1.0, [])
  end;
```

## Test Cases for the Code

```
interval_finder([1.0,2.0,3.0,4.0,5.0], 1.0, 0.0, 1.0, []);
```

## Algorithm Proof of Correctness and Proof of Running Time



# Shortest Paths

The Dijkstra's algorithm can be fooled through negative weights.

A - (5) -> B - (3) -> C - (-10) -> A

The shortest distance from A to B will be 3 instead of 

# Bipartite Graphs

A graph is bipartite if and only if the graph has no odd cycle.

## Definition of a Bipartite Graph

Let $G = (V,E)$ be a bipartite graph where $V = A \cup B$ such that $A \cap B = \emptyset $.  
Each edge is a set of nodes from a vertex of set $A$ to a vertex set $B$ such that $e = \{a, b\}$.

## Direct Proof of Necessary Condition

From any vertex $v \in G$ where $G$ has no odd cycles, define two sets of vertices where the shortest path from each vertex to $v$ is either even or odd.
$v$ will be in the set that the shortest path is even.
Resultingly, $A \cap B = \emptyset$.

## Proof by Contradiction of Sufficient Condtition

Given a graph $G$ with at least one odd cycle.
To maintain this odd cycle, the vertices along the path should include a vertex from $A$ followed by one from $B$ followed by one from $A$ and so forth.
$\forall k \in \{1, 2, 3, \ldots , n\}$ there are
$$
v_k \in \left\{ \begin{array}{lr} A: & k\ odd \\ B: & k\ even \end{array} \right. 
$$
In the case that we start with a node from $B$ for our cycle, then $B:\ k\ odd$.
If $n$ is odd for either of these sets, then the first and last vertices in the path are from the same set, contradicting the definition of bipartism for a graph.

For showing these two sets ($A, B$) satisfy $G = (A \cup B, E)$ to be bipartite, given two adjacent vertices in the same set, there should be a closed walk of odd length that includes these vertices one after the other, but since graphs containing a closed walk of odd length must also contain an odd cycle, no two vertices may be adjacent.
