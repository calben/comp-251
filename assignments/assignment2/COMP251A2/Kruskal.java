
import java.util.ArrayList;
import java.util.List;

public class Kruskal
{

    public static WGraph kruskal(WGraph g)
    {
        WGraph wgraph = new WGraph();
        DisjointSets set = new DisjointSets(g.getNbNodes());
        List<Edge> sortedEdges = g.listOfEdgesSorted();
        int numberOfTrees = g.getNbNodes();
        for (Edge e : sortedEdges)
        {
            if (numberOfTrees > 1 && IsSafe(set, e))
            {
                wgraph.addEdge(e);
                set.union(e.nodes[0], e.nodes[1]);
            }
        }
        return wgraph;
    }

    public static boolean IsSafe(DisjointSets p, Edge e)
    {
        return (p.find(e.nodes[0]) != p.find(e.nodes[1]));
    }

    public static void main(String[] args)
    {
        String file = args[0];
        WGraph g = new WGraph(file);
        WGraph t = kruskal(g);
        System.out.println(t);
    }
}
