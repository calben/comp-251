/*
 * Calem J Bendell
 * 260467886
 */

public class Multiply
{

    private static int randomInt(int size)
    {
        int maxval = (1 << size) - 1;
        return (int) (Math.random() * maxval);
    }

    public static int[] naive(int size, int x, int y)
    {
        if (size == 1)
        {
            // base case
            return new int[]
            {
                x * y, 1
            };
        } else
        {
            // recursive case
            double sizeDouble = (double) size / 2;
            int m = (int) (Math.ceil(sizeDouble));
            int a = (x / (int) Math.pow(2, m));
            int b = x % (int) Math.pow(2, m);
            int c = (y / (int) Math.pow(2, m));
            int d = y % (int) Math.pow(2, m);
            int a_result[] = naive(m, a, c);
            int b_result[] = naive(m, b, d);
            int c_result[] = naive(m, b, c);
            int d_result[] = naive(m, a, d);
            int result = (int) ((Math.pow(2, 2 * m) * a_result[0]) + (Math.pow(2, m)) * (c_result[0] + d_result[0])) + b_result[0];
            int cost = (a_result[1] + b_result[1] + c_result[1] + d_result[1] + (3 * m));
            return new int[]
            {
                result, cost
            };
        }
    }

    public static int[] karatsuba(int size, int x, int y)
    {
        if (size == 1)
        {
            // base case
            return new int[]
            {
                x * y, 1
            };
        } else
        {
            // recursive case
            double sizeDouble = (double) size / 2;
            int m = (int) (Math.ceil(sizeDouble));
            int a = x / (int) Math.pow(2, m);
            int b = x % (int) Math.pow(2, m);
            int c = y / (int) Math.pow(2, m);
            int d = y % (int) Math.pow(2, m);
            int a_result[] = karatsuba(m, a, c);
            int b_result[] = karatsuba(m, b, d);
            int c_result[] = karatsuba(m, a - b, c - d);
            int result = (int) ((Math.pow(2, 2 * m) * a_result[0]) + (Math.pow(2, m) * (a_result[0] + b_result[0] - c_result[0])) + b_result[0]);
            int cost = (a_result[1] + b_result[1] + c_result[1] + (6 * m));
            return new int[]
            {
                result, cost
            };
        }
    }

    public static void main(String[] args)
    {
        try
        {
            int maxRound = 20;
            int maxIntBitSize = 16;
            for (int size = 1; size < maxIntBitSize; size++)
            {
                int sumOpNaive = 0;
                int sumOpKaratsuba = 0;
                for (int round = 0; round < maxRound; round++)
                {
                    int x = randomInt(size);
                    int y = randomInt(size);
                    int[] resNaive = naive(size, x, y);
                    int[] resKaratsuba = karatsuba(size, x, y);
                    if (resNaive[0] != resKaratsuba[0])
                    {
                        throw new Exception("Return values do not match! (x=" + x + "; y=" + y + "; Naive=" + resNaive[0] + "; Karatsuba=" + resKaratsuba[0] + ")");
                    }
                    if (resNaive[0] != (x * y))
                    {
                        int myproduct = x * y;
                        throw new Exception("Evaluation is wrong! (x=" + x + "; y=" + y + "; Your result=" + resNaive[0] + "; True value=" + myproduct + ")");
                    }
                    sumOpNaive += resNaive[1];
                    sumOpKaratsuba += resKaratsuba[1];
                }
                int avgOpNaive = sumOpNaive / maxRound;
                int avgOpKaratsuba = sumOpKaratsuba / maxRound;
                System.out.println(size + "," + avgOpNaive + "," + avgOpKaratsuba);
            }
        } catch (Exception e)
        {
            System.out.println(e);
        }

    }
}
