
import java.io.*;
import java.lang.Double;
import java.util.*;
import java.util.Arrays;

/**
 *
 * YOUR COMMENTS ARE BAD
 * AND YOU SHOULD FEEL BAD
 *
 *
 */
public class COMP251HW1 {
    //Example for 10%, 30%, 50%, 70%, 80%, 90%, 100%, 120%, 150%, 200% --> This has to be updated
    public double[] ns = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0};
    /*Fields / methods for grading BEGIN*/
    private HashMap<Integer,String> pathMap;

    public int countCollisions(List<Long> x){
        Set<Long> uniqueSet = new HashSet<Long>(x);
        return uniqueSet.size();
    }

    public HashMap<Integer, String> getPaths() {
        return pathMap;
    }

    public void setPaths(HashMap<Integer, String> pathMap) {
        this.pathMap = pathMap;
    }
	/*Fields / methods for grading END*/

    public double[] generateRandomNumbers(int w, int size) {
        double[] resultArray = new double[size];
        if (getPaths() != null) {	//THIS PART WILL BE USED FOR GRADING
            String path = getPaths().get(size);
            File file = new File (path);
            Scanner scanner;
            try {
                scanner = new Scanner(file);
                int i = 0;
                while (scanner.hasNextLine() && i < resultArray.length) {
                    resultArray[i] = Double.parseDouble(scanner.nextLine());
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {

            for (int i = 0; i < size; i++) {
                resultArray[i] = Math.floor(Math.random()*(Math.pow(2, w)-1)); //cast to int to make it Integer
            }
        }
        return resultArray;
    }

    public double generateRandomNumberiInRange(double min, double max) {
        double res = min;
        while (res == min) {
            res = Math.floor(min + Math.random() * (max - min));
        }
        return res;
    }

    /**
     * method generateCSVOutputFile generates CSV File which contains row of x (first element is identificator "X"),
     * and one row for every experiment (ys - id with set of values)
     * Looks like this:
     * ================
     *  X,1,2,3
     *  E1,15,66,34
     *  E2,16,15,14
     *  E3,99,88,77
     * ================
     *
     * @param filePathName - absolute path to the file with name (it will be rewritten or created)
     * @param x - values along X axis, eg 1,2,3,4,5,6,7,8
     * @param ys - values for Y axis with the name of the experiment for different plots.
     */
    public void generateCSVOutputFile(String filePathName, double[] x, HashMap<String, double[]> ys) {
        File file = new File(filePathName);
        FileWriter fw;
        try {
            fw = new FileWriter(file);
            fw.append("X");
            for (double d: x) {
                fw.append("," + d);
            }
            fw.append("\n");
            for (Map.Entry<String, double[]> entry: ys.entrySet()) {
                fw.append(entry.getKey());
                double[] dTemp = entry.getValue();
                for (int i = 0, len = dTemp.length;i < len; i++) {
                    fw.append(","+dTemp[i]);
                }
                fw.append("\n");
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double[] divisionMethod (int d, int m, int w) {
        double[] ys = new double[ns.length];
        for (int it = 0, len = ns.length; it < len; it++) {
            int n = (int)(ns[it]*m);
            ys[it] = divisionMethodImpl (d, n, w);
        }
        return ys;
    }

    public double divisionMethodImpl (int d, int n, int w) {
       long D = (long) generateRandomNumberiInRange(Math.pow(2, d-1), Math.pow(2, d));
        double[] keys = generateRandomNumbers(w, n);
        long[] converted = new long[keys.length];
        for (int i = 0; i < keys.length; i++) {
            long tmp = (long) keys[i];
            tmp = tmp % D;
            converted[i] = tmp;
        }
        List<Long> obj = new ArrayList<Long>();
        for(long l : converted){
            obj.add(new Long(l));
        }
        Collections.sort(obj);
        System.out.println(obj);
        return countCollisions(obj);
    }


    /**
     * multiplicationMethod is the main method for multiplication method in hashing problem. It creates specific array ys, specifies A under with some validations, iterates over the set of n (defined by ns field) and for every n adds in ys array particular number of collisions
     *
     * It requires arguments:
     * m - number of slots, d and w - are such integers, that w > d
     *
     * It returns an array of number of collisions (for all n in range from 10% to 200% of m) - later in plotting phase - it is Y values for multiplicationMethod
     * @param d
     * @param m
     * @param w
     * @return ys for multiplication method {double[]}
     */
    public double[] multiplicationMethod (int d, int m, int w) {
        double[] ys = new double[ns.length];
        double y;
        double A = 0; // This line should be updated to assign a valid value to A
        for (int it = 0, len = ns.length; it < len; it++) {
            int n = (int)(ns[it]*m);
            y = multiplicationMethodImpl (d, n, w, A);
            if (y < 0) return null;
            ys[it] = y;
        }
        return ys;
    }


    /**
     * multiplicationMethodImpl is the particular implementation of the multiplication method.
     *
     * It requires arguments:
     * n - number of key to insert, d and w - are such integers, that w > d, A is a factor
     * @param d
     * @param n
     * @param w
     * @param A
     * @return number of collisions for particular configuration {double}
     */
    public double multiplicationMethodImpl (int d, int n, int w, double A) {
        A = generateRandomNumberiInRange(Math.pow(2, w-1), Math.pow(2, w));
        double D = java.lang.Math.pow(2, w);
        double[] keys = generateRandomNumbers(w, n);
        long[] converted = new long[keys.lsength];
        for (int i = 0; i < keys.length; i++) {
            long tmp = (long) ((A * keys[i]) % java.lang.Math.pow(2,w)) >> (w-d);
            converted[i] = tmp;
        }
        List<Long> obj = new ArrayList<Long>();
        for(long l : converted){
            obj.add(new Long(l));
        }
        Collections.sort(obj);
        return countCollisions(obj);
    }


    /**
     * TO STUDENT: MAIN method - WRITE/CHANGE code here (it should be compiled anyway!)
     * TO STUDENT: NUMBERS ARE RANDOM!
     * @param args
     */
    public static void main(String[] args) {
        int w = 0, d = 0, m = 0;
        if (args!= null && args.length>2) {
            w = Integer.parseInt(args[0]);
            d = Integer.parseInt(args[1]);
            m = Integer.parseInt(args[2]);
        } else {
            System.err.println("Input should be w d m (integers). Exit(-1).");
            System.exit(-1);
        }

        if (w<=d) {
            System.err.println("Input should contain w d (integers) such that w>d. Exit(-1).");
            System.exit(-1);
        }

        COMP251HW1 hf = new COMP251HW1();
        double[] yTemp;

        HashMap<String, double[]> ys = new HashMap<String, double[]>();
        System.out.println("===Division=Method==========");
        yTemp = hf.divisionMethod(d, m, w);
        if (yTemp == null) {
            System.out.println("Something wrong with division method. Check your implementation, formula and all its parameters.");
            System.exit(-1);
        }
        ys.put("divisionMethod", yTemp);
        for (double y: ys.get("divisionMethod")) {
            System.out.println(y);
        }

        System.out.println("============================");
        System.out.println("===Multiplication=Method====");
        yTemp = hf.multiplicationMethod(d, m, w);
        if (yTemp == null) {
            System.out.println("Something wrong with division method. Check your implementation, formula and all its parameters.");
            System.exit(-1);
        }
        ys.put("multiplicationMethod", yTemp);

        for (double y: ys.get("multiplicationMethod")) {
            System.out.println(y);
        }

        double[] x = new double[hf.ns.length];
        for (int it = 0, len = hf.ns.length; it < len; it++) {
            x[it] = (int)(hf.ns[it]*m);
        }

        hf.generateCSVOutputFile("hashFunctionProblem.csv", x, ys);
    }
}
