\contentsline {section}{\numberline {1}Good Facts to Know}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Trees}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Heaps}{1}{subsection.1.2}
\contentsline {section}{\numberline {2}Hashing}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Direct Address Table}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Chaining}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Hash Function}{1}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Open Addressing}{1}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Universal hashing}{1}{subsection.2.5}
\contentsline {section}{\numberline {3}Heaps}{1}{section.3}
\contentsline {subsection}{\numberline {3.1}BuildMaxHeap}{2}{subsection.3.1}
\contentsline {section}{\numberline {4}Trees}{2}{section.4}
\contentsline {subsection}{\numberline {4.1}Binary Trees}{2}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Binary Search Trees}{2}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}AVL Trees}{2}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Red-black trees}{2}{subsection.4.4}
\contentsline {section}{\numberline {5}Greedy algorithms}{2}{section.5}
\contentsline {subsection}{\numberline {5.1}Scheduling problem}{2}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Huffman encoding}{2}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Dijkstra's Substructure}{2}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Differences Between Bellman Ford and Dijkstra}{2}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Making Change with Coins}{2}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Rental Car Problem}{2}{subsection.5.6}
\contentsline {subsection}{\numberline {5.7}Lecture Hall Assignment Problem}{2}{subsection.5.7}
\contentsline {section}{\numberline {6}Graphs}{2}{section.6}
\contentsline {subsection}{\numberline {6.1}Edge Classification}{2}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Depth First Search}{3}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Breadth First Search}{3}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Parenthesis Theorem}{3}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}White Space Theorem}{3}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Directed Acyclic Graph}{3}{subsection.6.6}
\contentsline {subsubsection}{\numberline {6.6.1}Topological sort}{3}{subsubsection.6.6.1}
\contentsline {subsection}{\numberline {6.7}Minimum Spanning Tree}{3}{subsection.6.7}
\contentsline {subsubsection}{\numberline {6.7.1}Kruskal's Algorithm}{3}{subsubsection.6.7.1}
\contentsline {subsubsection}{\numberline {6.7.2}Prim's Algorithm}{4}{subsubsection.6.7.2}
\contentsline {subsection}{\numberline {6.8}Shortest Path Algorithms}{4}{subsection.6.8}
\contentsline {subsubsection}{\numberline {6.8.1}Dijkstra's Algorithm}{4}{subsubsection.6.8.1}
\contentsline {subsubsection}{\numberline {6.8.2}Bellman-Ford Algorithm}{4}{subsubsection.6.8.2}
\contentsline {subsubsection}{\numberline {6.8.3}Code}{4}{subsubsection.6.8.3}
\contentsline {section}{\numberline {7}Bipartite Graphs}{4}{section.7}
\contentsline {subsection}{\numberline {7.1}Gale Shapley}{4}{subsection.7.1}
\contentsline {section}{\numberline {8}Network Flow}{5}{section.8}
\contentsline {subsection}{\numberline {8.1}Ford-Fulkerson Algorithm}{5}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Dijkstra's Algorithm}{5}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Bellman Ford}{5}{subsection.8.3}
\contentsline {section}{\numberline {9}Dynamic programming}{5}{section.9}
\contentsline {subsection}{\numberline {9.1}Best alignment}{5}{subsection.9.1}
\contentsline {section}{\numberline {10}Some Proofs}{5}{section.10}
\contentsline {subsection}{\numberline {10.1}Minimum spanning trees}{5}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Shortest Paths}{6}{subsection.10.2}
\contentsline {subsection}{\numberline {10.3}Graphs}{6}{subsection.10.3}
\contentsline {subsection}{\numberline {10.4}DFS}{6}{subsection.10.4}
\contentsline {subsection}{\numberline {10.5}BFS}{6}{subsection.10.5}
\contentsline {section}{\numberline {11}Divide and Conquer}{6}{section.11}
\contentsline {subsection}{\numberline {11.1}Merge Sort}{6}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Karatsuba Multiplication}{6}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Block Matrix Multiplication}{6}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Strassen's Algorithm}{6}{subsection.11.4}
\contentsline {subsection}{\numberline {11.5}Dot Product}{6}{subsection.11.5}
\contentsline {subsection}{\numberline {11.6}Fast Fourier}{6}{subsection.11.6}
\contentsline {section}{\numberline {12}Amortized Analysis}{7}{section.12}
\contentsline {subsection}{\numberline {12.1}Aggregate Analysis}{7}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Accounting Method}{7}{subsection.12.2}
\contentsline {subsection}{\numberline {12.3}Potential Method}{7}{subsection.12.3}
\contentsline {subsection}{\numberline {12.4}Infinite Binary Counter}{7}{subsection.12.4}
\contentsline {section}{\numberline {13}Randomized Algorithms}{7}{section.13}
\contentsline {subsection}{\numberline {13.1}Karger Contraction Algorithm}{7}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Maximum 3 Satisfiability}{7}{subsection.13.2}
\contentsline {section}{\numberline {14}Common Recurrence Relations}{7}{section.14}
\contentsline {section}{\numberline {15}Master Theorem}{7}{section.15}
\contentsline {subsection}{\numberline {15.1}Proof}{7}{subsection.15.1}
\contentsline {subsection}{\numberline {15.2}Height of Recursion Tree}{7}{subsection.15.2}
\contentsline {section}{\numberline {16}Akra-Bazzi Theorem}{7}{section.16}
\contentsline {section}{\numberline {17}Red Black Trees}{7}{section.17}
\contentsline {subsection}{\numberline {17.1}Insertion}{8}{subsection.17.1}
\contentsline {subsection}{\numberline {17.2}Code}{8}{subsection.17.2}
\contentsline {section}{\numberline {18}Topological Sort}{9}{section.18}
\contentsline {section}{\numberline {19}Interval Scheduling}{9}{section.19}
\contentsline {section}{\numberline {20}Insert into AVL Tree}{9}{section.20}
\contentsline {section}{\numberline {21}Difference Between Prim's and Kruskal's Algorithm}{10}{section.21}
